package org.eclipse.etrice.core;

import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.etrice.core.room.ActorClass;
import org.eclipse.etrice.core.room.ActorRef;
import org.eclipse.etrice.core.room.RoomClass;
import org.eclipse.etrice.core.room.RoomFactory;
import org.eclipse.etrice.core.room.RoomModel;
import org.eclipse.etrice.core.tests.RoomInjectorProvider;
import org.eclipse.etrice.core.validation.StaticResourceValidator.ActorRefWalker;
import org.eclipse.etrice.core.validation.StaticResourceValidator.ActorRefWalker.ActorRefNode;
import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.XtextRunner;
import org.eclipse.xtext.validation.CheckMode;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(XtextRunner.class)
@InjectWith(RoomInjectorProvider.class)
public class StaticResourceValidatorTest extends TestBase {

	private static final String ROOM_FILE = "UsesResourcesImplicit.room";

	@Before
	public void setUp() {
		prepare(CoreTestsActivator.getInstance().getBundle());
	}

	@Test
	public void testNoUsesResources() {
		String sysName = "SystemNoUsesResources";
		RoomClass sys = findRoomClass(sysName, ROOM_FILE).orElseThrow();
		Diagnostic diag = getDiag(sys, CheckMode.NORMAL_AND_FAST, true);
		assertTrue(diag.getChildren().isEmpty());
	}

	@Test
	public void testDirectContainment() {
		String sysName = "SystemDirectContainment";
		RoomClass sys = findRoomClass(sysName, ROOM_FILE).orElseThrow();
		Diagnostic diag = getDiag(sys, CheckMode.NORMAL_AND_FAST, true);
		assertDiagMatch(diag,
			new ContainsSubstringsPredicate(Diagnostic.ERROR,
				sysName,
				"Resource \"R1\"",
				"in ref subsystem/a2:etrice\\.api\\.annotations\\.A",
				"first encountered in subsystem/a1:etrice\\.api\\.annotations\\.A"),
			new ContainsSubstringsPredicate(Diagnostic.ERROR,
				sysName,
				"Resource \"R2\"",
				"in ref subsystem/a2:etrice\\.api\\.annotations\\.A",
				"first encountered in subsystem/a1:etrice\\.api\\.annotations\\.A"),
			new ContainsSubstringsPredicate(Diagnostic.ERROR,
				sysName,
				"Resource \"R1\"",
				"in ref subsystem/b:etrice\\.api\\.annotations\\.B",
				"first encountered in subsystem/a1:etrice\\.api\\.annotations\\.A")
		);
	}

	@Test
	public void testNested() {
		String sysName = "SystemNested";
		RoomClass sys = findRoomClass(sysName, ROOM_FILE).orElseThrow();
		Diagnostic diag = getDiag(sys, CheckMode.NORMAL_AND_FAST, true);
		assertDiagMatch(diag,
			new ContainsSubstringsPredicate(Diagnostic.ERROR,
				sysName,
				"Resource \"R1\"",
				"in ref subsystem/container/a:etrice\\.api\\.annotations\\.A",
				"first encountered in subsystem/container:etrice.api.annotations.ContainerA")
		);
	}

	@Test
	public void testSeparateInstanceBranches() {
		String sysName = "SystemSeparateInstanceBranches";
		RoomClass sys = findRoomClass(sysName, ROOM_FILE).orElseThrow();
		Diagnostic diag = getDiag(sys, CheckMode.NORMAL_AND_FAST, true);
		assertDiagMatch(diag,
			new ContainsSubstringsPredicate(Diagnostic.ERROR,
				sysName,
				"Resource \"R3\"",
				"in ref subsystem/container2/c:etrice\\.api\\.annotations\\.C",
				"first encountered in subsystem/container1/c:etrice\\.api\\.annotations\\.C")
		);
	}

	@Test
	public void testCyclic() {
		String sysName = "SystemCyclic";
		RoomClass sys = findRoomClass(sysName, ROOM_FILE).orElseThrow();
		Diagnostic diag = getDiag(sys, CheckMode.NORMAL_AND_FAST, true);
		assertNoDiagMatch(diag,
			new ContainsSubstringsPredicate(Diagnostic.ERROR,
				sysName,
				"Resource",
				"in ref",
				"first encountered in")
		);
	}

	@Test
	public void testIndirectInheritedCyclic() {
		String sysName = "SystemIndirectInheritedCyclic";
		RoomClass sys = findRoomClass(sysName, ROOM_FILE).orElseThrow();
		Diagnostic diag = getDiag(sys, CheckMode.NORMAL_AND_FAST, true);
		assertDiagMatch(diag,
			new ContainsSubstringsPredicate(Diagnostic.ERROR,
				sysName,
				"Resource \"R1\"",
				"in ref subsystem/container/descendant/ancestor/b:etrice\\.api\\.annotations\\.B",
				"first encountered in subsystem/container/ancestor/b:etrice\\.api\\.annotations\\.B")
		);
	}

	@Test
	public void testNestedMultipleInstances() {
		String sysName = "SystemNestedMultipleInstances";
		RoomClass sys = findRoomClass(sysName, ROOM_FILE).orElseThrow();
		Diagnostic diag = getDiag(sys, CheckMode.NORMAL_AND_FAST, true);
		assertDiagMatch(diag,
			new ContainsSubstringsPredicate(Diagnostic.ERROR,
				sysName,
				"Resource \"R1\"",
				"in ref subsystem/outer1Ref/inner/b:etrice\\.api\\.annotations\\.B",
				"first encountered in subsystem/outer2Ref/inner/b:etrice\\.api\\.annotations\\.B")
		);
	}

	@Test
	public void testDuplicateInActorClass() {
		String actorName = "Duplicate";
		RoomClass actor = findRoomClass(actorName, ROOM_FILE).orElseThrow();
		Diagnostic diag = getDiag(actor);
		assertDiagMatch(diag,
				new ContainsSubstringsPredicate(Diagnostic.WARNING,
					"Implicit resource \"R\" is already declared"));
	}

	@Test
	public void testInvalidNames() {
		String actorName = "InvalidNames";
		RoomClass actor = findRoomClass(actorName, ROOM_FILE).orElseThrow();
		Diagnostic diag = getDiag(actor);
		DiagnosticPredicate predicateInvalidName = new ContainsSubstringsPredicate(Diagnostic.ERROR,
				"Implicit resource name contains invalid");
		assertDiagMatch(diag.getChildren().get(0),
				predicateInvalidName);
		assertDiagMatch(diag.getChildren().get(1),
				predicateInvalidName);
	}

	@Test
	public void testReplicatedRefInActorClass() {
		String actorName = "Replicated";
		RoomClass actor = findRoomClass(actorName, ROOM_FILE).orElseThrow();
		Diagnostic diag = getDiag(actor);
		assertDiagMatch(diag,
				new ContainsSubstringsPredicate(Diagnostic.ERROR,
					"Resource",
					"must have multiplicity of 1"));
	}

	private Optional<RoomClass> findRoomClass(String name, String roomFile) {
		Resource res = getResource(roomFile);
		RoomModel model = (RoomModel) res.getContents().get(0);
		Optional<RoomClass> first = roomHelpers.getRoomClasses(model, RoomClass.class)
				.filter(actor -> actor.getName().equals(name)).findFirst();
		return first;
	}

	@Test
	public void testActorRefIteratorUpToDepthLimit() {
		ActorClass rootUpToLimit = buildContainmentHierarchy(ActorRefWalker.ActorRefWalkerIterator.MAX_DEPTH);
		Iterator<ActorRefNode> iter = new ActorRefWalker(rootUpToLimit, roomHelpers).iterator();
		while (iter.hasNext()) {
			iter.next();
		}
	}

	@Test
	public void testActorRefIteratorOverDepthLimit() {
		ActorClass rootOverLimit = buildContainmentHierarchy(ActorRefWalker.ActorRefWalkerIterator.MAX_DEPTH + 1);
		Iterator<ActorRefNode> iter = new ActorRefWalker(rootOverLimit, roomHelpers).iterator();
		assertThrows(RuntimeException.class, () -> {
			while (iter.hasNext()) {
				iter.next();
			}
		});
	}

	/**
	 * Builds a hierarchy of ActorRefs to a specified depth (root ActorClass is zero-depth) 
	 * @param depth
	 * @return
	 */
	private ActorClass buildContainmentHierarchy(int depth) {
		ActorClass root = RoomFactory.eINSTANCE.createActorClass();
		root.setName("root");

		List<ActorClass> classes = IntStream.range(0, depth).mapToObj(i -> {
			ActorClass ac = RoomFactory.eINSTANCE.createActorClass();
			ac.setName("Actor"+Integer.toString(i));
			return ac;
		}).collect(Collectors.toList());
		
		for (int i = 1; i < classes.size(); i++) {
			ActorRef ref = RoomFactory.eINSTANCE.createActorRef();
			ref.setName("sub");
			ref.setType(classes.get(i));
			classes.get(i-1).getActorRefs().add(ref);
		}

		ActorRef ref = RoomFactory.eINSTANCE.createActorRef();
		ref.setName("sub");
		ref.setType(classes.get(0));
		root.getActorRefs().add(ref);
		return root;
	}
}

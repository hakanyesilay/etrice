Prism.languages.etmap = {
	'comment': {
		pattern: /(?:\/\*[\s\S]*?\*\/)|(?:\/\/.*)/,
		greedy: true
	},
	'string': {
		pattern: /"(?:\\(?:\r\n|[\s\S])|[^"\\\r\n])*"/,
		greedy: true
	},
	'punctuation': /[{}]/,
    'operator': /=|->/,
	'keyword': /\b(?:import|ThreadMapping|Mapping|from|model|SubSystemMapping|MappingModel)\b/
};

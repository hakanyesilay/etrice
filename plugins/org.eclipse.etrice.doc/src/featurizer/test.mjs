// @ts-check

import {strictEqual, deepStrictEqual} from 'node:assert/strict';
import {test} from 'node:test';
import fs from 'fs';
import unified from 'unified';
import remarkParse from 'remark-parse';
import remarkMdx from 'remark-mdx';
import {parseFeatures, checkFeatures} from './md-featurizer.mjs';

test('md-featurizer test', async (t) => {
	const buffer = fs.readFileSync('./src/featurizer/test-features.md')
	
	const root = unified()
		.use(remarkParse)
		.use(remarkMdx)
		.parse(buffer);
	console.debug('Parsed markdown:');
	console.debug(JSON.stringify(root));
	
	/** @type {Array<import('./md-featurizer.mjs').Feature>} */
	const expectedFeatures = [
		{
			name: 'TestFeatures',
			isPackage: true,
			relations: [
				{ type: 'Contains:', feature: 'Feature1' },
				{ type: 'Contains:', feature: 'Feature2' },
				{ type: 'Contains:', feature: 'Feature3' }
			]
		},
		{
			name: 'Feature1',
			isPackage: false,
			relations: [
				{ type: 'Is a:', feature: 'Feature2' },
				{ type: 'Is of type:', feature: 'Feature2' },
				{ type: 'Contains:', feature: 'Feature2' },
				{ type: 'Uses:', feature: 'Feature2' },
				{ type: 'Views:', feature: 'Feature2' },
				{ type: 'Edits:', feature: 'Feature2' },
				
				{ type: 'Is used by:', feature: 'Asdf' },	// error: unknown feature
				{ type: 'Qwer:', feature: 'Feature3' }		// error: unrecognized relation type
			]
		},
		{
			name: 'Feature2',
			isPackage: false,
			relations: [
				{ type: 'Inheriting features:', feature: 'Feature1' },
				{ type: 'Typecasts:', feature: 'Feature1' },
				{ type: 'Is contained in:', feature: 'Feature1' },
				{ type: 'Is used by:', feature: 'Feature1' },
				{ type: 'Is shown by:', feature: 'Feature1' },
				{ type: 'Is edited by:', feature: 'Feature1' }
			]
		},
		{
			name: 'Feature3',
			isPackage: false,
			relations: [
				{ type: 'Views:', feature: 'Feature2' }		// error: missing opposite relation
			]
		}
	];

	strictEqual(root.type, 'root');
	const actualFeatures = parseFeatures(/** @type {import('mdast').Root} */ (root));
	deepStrictEqual(actualFeatures, expectedFeatures);
	console.debug('Parsed features:');
	console.debug(JSON.stringify(actualFeatures));

	const errors = checkFeatures(actualFeatures);
	console.debug('Produced error messages:');
	errors.forEach(error => console.debug(error));
	strictEqual(errors.length, 3);
});

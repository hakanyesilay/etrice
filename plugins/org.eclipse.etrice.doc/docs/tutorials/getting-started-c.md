Getting Started C
=================

In this tutorial you will have a first look at a very simple eTrice model. The goal is to learn the work flow of eTrice and to understand a few basic features of ROOM.

## Preconditions

- Working C development environment with Eclipse CDT
	- C compiler: MinGW/GCC (Windows) or Posix/GCC (Linux)
	- Installed C/C++ Development Tools (CDT) in Eclipse

- Activated *Build Automatically* option in Eclipse: *Project -&gt; Build Automatically*

- Recommended: Activate automatic resource refresh in Eclipse: *Window -&gt; Preferences -&gt; General -&gt; Workspace*
	- *Refresh using native hooks or polling*
	- *Refresh on access*

- Optional: Install [Trace2UML](https://wiki.astade.de/), an Open Source MSC viewer
	- [Windows download site](https://wiki.astade.de/dokuwiki/doku.php?id=download:index)
	- [Linux package](https://wiki.astade.de/dokuwiki/doku.php?id=download:index)

## How to install the MinGW compiler under Windows

1. Go to the official [mingw64 download site](https://www.mingw-w64.org/downloads/) and select a pre-built windows toolchain from the list, for example one from [WinLibs.com](https://winlibs.com/)

2. Select a variant to use (for example 64-bit using POSIX threads, SEH, and UCRT) and download the corresponding binary archive.

3. Extract the contents of the binary archive to a local install directory, preferably without spaces in the path.

4. Set the environment variable path to include the MinGW directory. Open the windows menu with "windows key", type "Control Panel" and press Enter. 
	- At the top right of the control panel window there should be a search box. Select it and type "Advanced system settings" and then click on the search result "View advanced system settings".
	
	- Click on "Environment Variables" and double click on the user variable "Path". In the new "Edit environment variable window", enter a new absolute path pointing to the "bin" subfolder of the local install directory, i.e. `[local install directory]/bin`. Press OK on all open settings windows to close them.

5. To verify that your installation is working, run the following commands and confirm that the outputs match as follows. It is possible that a restart may necessary to confirm the changes.

```console
> gcc --version
g++ (GCC) X.X.X
......
 
> g++ --version
g++ (GCC) X.X.X
......
 
> gdb --version
GNU gdb (GDB) X.X.X
......
```

## Create template project

After installation of eTrice in Eclipse, we can use the wizard to create the *template.c* project, which is a good starting point for new ROOM applications. Select the menu *File -&gt; New -&gt; Other* (Ctrl+N) and choose *eTrice C Template Project*. To complete the wizard press *Next* and on the next page *Finish*.

![image](../images/013-wizard.png)

This will add the three projects into your workspace:

- the library projects *runtime.c* and *modellib.c*
- our application project *template.c*.

Now open the *TemplateModel.room* in the model folder of *template.c*.

![image](../images/013-after-wizard.png)

In this step we are introduced to the concept of Actors, a fundamental building block in ROOM, and how they are used in eTrice. In essence, an Actor is a representation of an object as a logical machine with defined behavior. Its structure, behavior and protocols are all defined with in an element called an ActorClass. See the [documentation on Actors](../concepts/actors.md) for further information.

The model contains an ActorClass *TopActor*, which is currently the only active actor. TopActor has a state machine that is intended to output a simple HelloWorld statement. To inspect its behavior graphically, we can open the behavior diagram by right clicking on *Outline View -&gt; TopActor -&gt; Edit Behavior*. Alternatively we can place the cursor within the textual notation and press Alt+B.

![image](../images/013-open-hello-fsm.png)

The state machine has an initial transition that leads to *helloState*. The state defines an entry code, which is executed during the transition. We can view the details by hovering over the state and edit the code with a double-click or right click and *Edit State*.

![image](../images/013-hello-fsm.png)

## Generate and run application

Now we are ready to translate the model into an executable application.
The first step is to generate source files from the model.
There are two model projects present in the workspace: the eTrice model library (org.eclipse.etrice.modellib.c), and the application project itself (org.eclipse.etrice.template.c).
Initially we need to run the eTrice model generator for both projects.
For subsequent model changes, only the affected project needs to be regenerated (the eTrice model library typically does not change unless there is an update to the library between eTrice versions).
Run the eTrice model generator by right clicking on file

![](../icons/file_obj.gif)`org.eclipse.etrice.modellib.c/generate_modellib_c.launch` &rarr; `Run As` &rarr; `generate_modellib_c`

and then by doing the same for the file

![](../icons/file_obj.gif)`org.eclipse.etrice.template.c/generate_Template.launch` &rarr; `Run As` &rarr; `generate_Template`

After that we can build and run the application by right clicking on file

![](../icons/file_obj.gif)`org.eclipse.etrice.template.c/run_Template_Windows.launch` &rarr; `Run As` &rarr; `run_Template_Windows`

This will trigger the build process, carried out by Eclipse CDT and then subsequently start our application.

|                                               |                                      |                                       |
|:---------------------------------------------:|:------------------------------------:|:-------------------------------------:|
| ![image](../images/014-generate-modellib.png) | ![image](../images/014-generate.png) | ![image](../images/014-build_run.png) |
| <center>[0. Invoke generator for modellib (once after project creation)](../images/014-generate-modellib.png)</center> | <center>[1. Invoke generator for application](../images/014-generate.png)</center> | <center>[2. Build and run application](../images/014-build_run.png)</center> |


The output of our application is logged in the Console View and it shows the "Hello World". By typing "quit" on the prompt and pressing enter the application terminates regularly.

![image](../images/013-quit-hello-msc.png)

## Open the Message Sequence Chart

After termination, we can inspect the behavior of our application. It is recorded in the form of a MSC (Message Sequence Chart) and can now be used for debugging or documentation purposes. Open *msc.seq* or *subSystemRef_Async.seq* in the folder *log* using the tool Trace2UML (if the file is not present, try to refresh (F5) the folder *log*).

:::note

The Trace2UML (Open Source tool) download links can be found in the [Preconditions section](#preconditions) of this page.

:::

Yet the MSC is nearly empty having not recorded any interaction between actors. It shows that *topActor* (full instance path */LogSys/subSystemRef/topActor*) has assumed *helloState*. In the next PingPong tutorial we are going to create a more sophisticated and vivid eTrice application introducing actor building blocks and message protocols.


![image](../images/013-hello-msc.png)

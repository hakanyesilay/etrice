eTrice Java Projects
====================

eTrice Java projects use the Eclipse JDT build to build and deploy an eTrice application.

## Eclipse JDT Build

The eTrice new project wizard requires the `org.eclipse.etrice.runtime.java` project in the workspace and adds a dependency to it.

If the project uses other eTrice projects (e.g. the `org.eclipse.etrice.modellib.java`) they have to be added to the Java build path as well.

The eTrice new project wizard creates the following files for the JDT build

-   a ROOM model file with exemplary classes

-   a simple physical model

-   a model mapping the logical entities of the ROOM model to the physical entities

-   a launch configuration that invokes the eTrice Java code generator for the new models

-   a launch configuration that launches the main method of the generated code

If “build automatically” is chosen the newly created model can be generated and launched with just two clicks.

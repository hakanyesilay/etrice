# ModelEditors
All aspects of the ROOMLanguage can be edited by full-blown textual editors. In addition, graphical editing is provided for the structural and behavioral part of ActorClasses.


| Features |     |
| -------- | --- |
| Contains: | [TextualROOMEditor][], [GraphicalStructureEditor][], [GraphicalBehaviorEditor][] |

## GraphicalBehaviorEditor
The GraphicalBehaviorEditor allows to edit the ActorClass' StateMachine. It is possible to create (hierarchical) states and transitions to model complex behavior in a convenient way.

![GraphicalBehaviorEditor](../images/300-GraphicalBehaviorEditor.png)


| Features |     |
| -------- | --- |
| Edits: | [StateMachine][] |



---


## GraphicalStructureEditor
The Structure Editor allows to edit the ActorClass' Structure in a convenient way. It is possible to create and arrange actor references and ports and to create bindings and layer connections.

![GraphicalStructureEditor](../images/300-GraphicalStructureEditor.png)


| Features |     |
| -------- | --- |
| Contains: | [StructureEditorPalette][], [ActorRefPropertyDialog][], [PortPropertyDialog][], [SPPPropertyDialog][] |
| Edits: | [ActorClass][], [ActorRef][], [Port][], [SAP][], [Binding][], [LayerConnection][] |



---


### ActorRefPropertyDialog
A dialog to edit properties of an ActorRef.

The dialog is used to edit an existing ActorRef of an ActorClass. It is also shown when creating a new one.

![ActorRefDialog](../images/300-ActorRefDialog.png)



| Features |     |
| -------- | --- |
| Edits: | [ActorRef][] |

| Feature Usage |     |
| ------------- | --- |
| Is contained in: | [GraphicalStructureEditor][] |


---


### PortPropertyDialog
A dialog to edit properties of an Port.

The dialog is used to edit an existing Port of an ActorClass. It is also shown when creating a new one.

![PortDialog](../images/300-PortDialog.png)



| Features |     |
| -------- | --- |
| Edits: | [Port][] |

| Feature Usage |     |
| ------------- | --- |
| Is contained in: | [GraphicalStructureEditor][] |


---


### SPPPropertyDialog
A dialog to edit properties of a SPP.

The dialog is used to edit an existing SPP of an ActorClass. It is also shown when creating a new one.

![SPPDialog](../images/300-SPPDialog.png)



| Features |     |
| -------- | --- |
| Edits: | [SPP][] |

| Feature Usage |     |
| ------------- | --- |
| Is contained in: | [GraphicalStructureEditor][] |


---


### StructureEditorPalette
The palette creates central structural elements of an ActorClass.

Selecting an entry from the palette and clicking into the diagram, creates the element at the current position.

![StructurePalette](../images/300-StructurePalette.png)




| Feature Usage |     |
| ------------- | --- |
| Is contained in: | [GraphicalStructureEditor][] |


---


## TextualROOMEditor
Textual model editor

![TextualROOMEditor](../images/300-TextualROOMEditor.png)


| Features |     |
| -------- | --- |
| Contains: | [OutlineView][] |
| Edits: | [ROOMLanguage][] |



---


### OutlineView
Displays an overview of all elements in the textual editor.

Shows the structure of the current opened model in the textual editor. Select the 'Link with Editor' option to synchronize the selection of elements between editor and outline view. This enables a convenient navigation.

![OutlineView](../images/300-OutlineView.png)




| Feature Usage |     |
| ------------- | --- |
| Is contained in: | [TextualROOMEditor][] |


---


[TextualROOMEditor]: #textualroomeditor
[OutlineView]: #outlineview
[GraphicalBehaviorEditor]: #graphicalbehavioreditor
[GraphicalStructureEditor]: #graphicalstructureeditor
[StructureEditorPalette]: #structureeditorpalette
[ActorRefPropertyDialog]: #actorrefpropertydialog
[PortPropertyDialog]: #portpropertydialog
[SPPPropertyDialog]: #spppropertydialog

[ROOMLanguage]: room-language.md
[ActorClass]: room-language.md#actorclass
[StateMachine]: room-language.md#statemachine
[ActorRef]: room-language.md#actorref
[Binding]: room-language.md#binding
[LayerConnection]: room-language.md#layerconnection
[Port]: room-language.md#port
[SAP]: room-language.md#sap
[SPP]: room-language.md#spp

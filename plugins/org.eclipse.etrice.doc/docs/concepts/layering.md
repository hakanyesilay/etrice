Layering
========

## Description

In addition to the actor containment hierarchies, layering provides another method to hierarchically structure a software system. Layering and actor hierarchies with port to port connections can be mixed on every level of granularity.

1.  an actor class can define a Service Provision Point (SPP) to publish a specific service, defined by a protocol class

2.  an actor class can define a Service Access Point (SAP) if it needs a service, defined by a protocol class

3.  for a given actor hierarchy, a LayerConnection defines which SAP will be satisfied by (connected to) which SPP

## Notation

For the graphical and textual notation refer to the following table:
		
<table class="table">
<thead>
<tr>
<th><strong>Graphical Notation</strong></th>
<th><strong>Textual Notation</strong></th>
</tr>
</thead>
<tbody>
<tr>
<td>

![](../images/040-LayeringModel.png)

</td>
<td>

```room
ActorClass Mode1 {
	Structure {
		ActorRef Services: ServiceLayer
		ActorRef Application: ApplicationLayer
		
		LayerConnection ref Application satisfied_by Services.timer
		LayerConnection ref Application satisfied_by Services.digitalIO
	}
}
```

</td>
</tr>
</tbody>
<caption>The layer connections in this model define which services are provided by the <i>ServiceLayer</i> (<i>digitalIO</i> and <i>timer</i>)</caption>
</table>
	
<table  class="table">
<tbody>
<tr>
<td>

![](../images/040-LayeringServiceLayer.png)

</td>
<td>

```room
ActorClass ServiceLayer {
	Interface {
		SPP timer: TimerProtocol
		SPP digitalIO: DigitalIOProtocol
	}
	Structure {
		ActorRef Timer: TimerService
		ActorRef DigIO: DifitalIOService
		LayerConnection relay_sap timer satisfied_by Timer.timer
		LayerConnection relay_sap digitalIO satisfied_by DigIO.digitalIO
	}
}
```

</td>
</tr>
</tbody>
<caption>The implementation of the services (SPPs) can be delegated to sub actors. In this case the actor <i>ServiceLayer</i> relays (delegates) the implementation services <i>digitalIO</i> and <i>timer</i> to sub actors</caption>
</table>
<table class="table">
<tbody>
<tr>
<td>

![](../images/040-LayeringApplicationLayer.png)

</td>
<td>

```room
ActorClass ApplicationLayer {
	Structure {
		ActorRef function1: A
		ActorRef function2: B
		ActorRef function3: C
		ActorRef function4: D
	}
}

ActorClass A {
	Structure {
		SAP timerSAP: TimerProtocol
	}
}

ActorClass B {
	Structure {
		SAP timerSAP: TimerProtocol
		SAP digitalSAP: DigitalIOProtocol
	}
}
```

</td>
</tr>
</tbody>
<caption>Every Actor inside the <i>ApplicationLayer</i> that contains an SAP with the same protocol as <i>timer</i> or <i>digitalIO</i> will be connected to the specified SPP</caption>
</table>

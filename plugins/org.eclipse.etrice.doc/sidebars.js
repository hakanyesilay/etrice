// @ts-check

/** @type {import('@docusaurus/plugin-content-docs').SidebarsConfig} */
const sidebars = {
	docs: [
		{
			type: 'category',
			label: 'Introduction',
			items: [
				'introduction/etrice-overview',
				'introduction/room-language'
			],
		},
		{
			type: 'category',
			label: 'Tutorials',
			link: {
				type: 'doc',
				id: 'tutorials/README'
			},
			items: [
				'tutorials/getting-started-c',
				'tutorials/getting-started-cpp',
				'tutorials/ping-pong',
				'tutorials/troubleshooting'
			]
		},
		'standalone-generators',
		{
			type: 'category',
			label: 'Examples',
			link: {
				type: 'doc',
				id: 'examples/README'
			},
			items: [
				'examples/c-examples',
				'examples/java-examples'
			]
		},
		{
			type: 'category',
			label: 'ROOM Concepts',
			link: {
				type: 'doc',
				id: 'concepts/README'
			},
			items: [
				'concepts/actors',
				'concepts/protocols',
				'concepts/ports',
				'concepts/data-class',
				'concepts/layering',
				'concepts/finite-state-machines'
			]
		},
		{
			type: 'category',
			label: 'eTrice Features',
			items: [
				'features/model-navigation',
				'features/java-projects',
				'features/automatic-diagram-layout',
				'features/annotations',
				'features/enumerations',
				'features/model-relations',
				'features/interface-contracts',
				'features/interrupt-events'
			]
		},
		{
			type: 'category',
			label: 'Feature Reference',
			items: [
				'reference/room-language',
				'reference/model-editors',
				'reference/code-generators'
			]
		},
		{
			type: 'category',
			label: 'Dave Integration',
			link: {
				type: 'doc',
				id: 'dave/README'
			},
			items: [
				'dave/getting-started-without-operating-system',
				'dave/getting-started-with-freertos'
			]
		},
		'target-integration',
		'resource-requirements',
		{
			type: 'category',
			label: 'Developer\'s Reference',
			items: [
				'developers/architecture',
				'developers/component-overview'
			]
		},
		'release-notes'
	],
};

module.exports = sidebars;

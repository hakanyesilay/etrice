eTrice Documentation
====================

**Prerequisites:**
* [Node](https://nodejs.org/en) (version >= 16.14)
* [Docusaurus](https://docusaurus.io/) (run `npm install` in the project directory)

## Project Structure
```sh
org.eclipse.etrice.doc
├───build                       # Docusaurus build output
├───docs                        # markdown content for online documentation
├───keyword-hover               # html content for Eclipse keyword hover
├───src
|   ├───featurizer              # Javascript md-featurizer sources
│   ├───org/eclipse/etrice/doc  # Java code for Eclipse keyword hover
│   └───theme                   # Docusaurus theme config (syntax highlighting)
├───static                      # static files for Docusaurus
│   └───img
├───docusaurus.config.js        # Docusaurus configuration
├───package.json
├───README.md
└───sidebar.js                  # Docusaurus sidebar configuration
```

## Artifacts

- Online Documentation Site
- Eclipse Keyword Hover

## How To Edit
The documentation content resides in folder `./docs`.
The output folder for the complete generated site is `./build`.

### Build

To build the documentation there are two use cases:

- Incremental build while editing the *.md sources.
Execute `npm run start` to start a development server.
- One time build e.g. after pulling the repo.
Run `npm run build` to create a production build of the online documentation.

**NOTE:** Docusaurus won't warn if the development server port is already bound to something else.
If there are issues accessing the localhost documentation site, it might help to try a different port using the `--port` argument, e.g.:

````
npm run start -- --port [PORT_NUMBER]
````

### Feature Model

Previously, the reference documentation was described by a special dsl called 'featurizer'.
Because mainting the featurizer meant additional effort and because this made editing the content a bit more involved, the featurizer has been retired.

The reference documentation in `docs/reference` was initially generated from such a feature model.
These markdown files are now edited directly, so please make sure to keep the overall structure of the documents as shown below.
The feature relations can be automatically checked for consistency via `npm run md-featurizer`.

````markdown
### Example Feature

<!-- description -->
Lorem ipsum.

<!-- text -->
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

<!-- properties -->
| Properties | Values |
| ---------- | ------ |
| example_property | *true, false* |

<!-- relations -->
| Features |     |
| -------- | --- |
| Is a:         | [OtherFeature][] |
| Is of type:   | [OtherFeature][] |
| Contains:     | [OtherFeature][] |
| Uses:         | [OtherFeature][] |
| Views:        | [OtherFeature][] |
| Edits:        | [OtherFeature][] |

<!--  reverse relations -->
| Feature Usage |     |
| ------------- | --- |
| Inheriting features:  | [OtherFeature][] |
| Typecasts:            | [OtherFeature][] |
| Is contained in:      | [OtherFeature][] |
| Is used by:           | [OtherFeature][] |
| Is shown by:          | [OtherFeature][] |
| Is edited by:         | [OtherFeature][] |

**Example**:

<!-- example -->
```room
ActorClass ExampleActor {}
```

<!-- link reference definitions -->
[Example Feature]: #example-feature
[Other Feature]: #other-feature
````

The same is true for the Eclipse hover content in folder `keyword-hover` which is now also edited directly.
Eclipse (context) help is no longer provided for eTrice.

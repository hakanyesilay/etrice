/*******************************************************************************
 * Copyright (c) 2012 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		Henrik Rentz-Reichert (initial contribution)
 * 
 *******************************************************************************/

module org.eclipse.etrice.core.etmap.GenerateETMap

import org.eclipse.xtext.xtext.generator.*
import org.eclipse.xtext.xtext.generator.model.project.*

var projectName = "org.eclipse.etrice.core.etmap"

Workflow {
    component = XtextGenerator {
    	configuration = {
    		project = StandardProjectConfig {
    			baseName = projectName
    			rootPath = ".."
    			runtimeTest = {
    				enabled = true
    				root = "../../tests/${projectName}.tests"
    			}
    			eclipsePlugin = {
    				enabled = true
    			}
    			createEclipseMetaData = true
    		}
    		code = {
				preferXtendStubs = false
    		}
    	}
    	language = StandardLanguage {
    		name = "org.eclipse.etrice.core.etmap.ETMap"
    		fileExtensions = "etmap"
    		referencedResource = "platform:/resource/org.eclipse.etrice.core.common/model/Base.genmodel"
    		referencedResource = "platform:/resource/org.eclipse.etrice.core.room/model/Room.genmodel"
    		referencedResource = "platform:/resource/org.eclipse.etrice.core.etphys/model/ETPhys.genmodel"
    		referencedResource = "platform:/resource/${projectName}/model/ETMap.genmodel"
    		serializer = {
    			generateStub = false
    		}
    		validator = {
    			composedCheck = "org.eclipse.xtext.validation.ImportUriValidator"
    		}
    		generator = {
				generateStub = false
			}
			junitSupport = {
				generateStub = false
			}
    	}
    }
    // call the ecore generator only after running the Xtext generator because of directory cleaning issues and also this bug: https://bugs.eclipse.org/bugs/show_bug.cgi?id=550756
    component = org.eclipse.emf.mwe2.ecore.EcoreGenerator {
    	genModel = "platform:/resource/${projectName}/model/ETMap.genmodel"
    	srcPath = "platform:/resource/${projectName}/src-gen"
    }
}
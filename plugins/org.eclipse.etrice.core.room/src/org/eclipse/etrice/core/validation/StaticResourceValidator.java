/*******************************************************************************
 * Copyright (c) 2024 protos software gmbh (http://www.protos.de).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * CONTRIBUTORS:
 * 		epaen (initial contribution)
 * 
 *******************************************************************************/

package org.eclipse.etrice.core.validation;

import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.etrice.core.common.base.BasePackage;
import org.eclipse.etrice.core.room.ActorContainerClass;
import org.eclipse.etrice.core.room.ActorContainerRef;
import org.eclipse.etrice.core.room.ActorRef;
import org.eclipse.etrice.core.room.LogicalSystem;
import org.eclipse.etrice.core.room.RoomModel;
import org.eclipse.etrice.core.room.RoomPackage;
import org.eclipse.etrice.core.room.StructureClass;
import org.eclipse.etrice.core.room.util.RoomHelpers;
import org.eclipse.etrice.core.room.util.StaticResourceHelpers;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.resource.IResourceDescription;
import org.eclipse.xtext.resource.IResourceDescription.Delta;
import org.eclipse.xtext.resource.IResourceDescriptions;
import org.eclipse.xtext.resource.impl.IsAffectedExtension;
import org.eclipse.xtext.validation.AbstractDeclarativeValidator;
import org.eclipse.xtext.validation.Check;
import org.eclipse.xtext.validation.CheckType;
import org.eclipse.xtext.validation.EValidatorRegistrar;

import com.google.inject.Inject;

/**
 * Checks that implicit resources declared using the UsesResources annotation
 * only exist for one ActorInstance per LogicalSystem. NOTE: assumes currently
 * that all ActorRefs are fixed when calculating resource acquisition.
 */
public class StaticResourceValidator extends AbstractDeclarativeValidator {

	@Inject
	RoomHelpers roomHelpers;

	@Inject
	StaticResourceHelpers staticResourceHelpers;

	@Inject
	IQualifiedNameProvider nameProvider;

	static final String REGEX_VALID_RESOURCE_NAME = "^[a-zA-Z_]\\w*";

	@Check(CheckType.FAST)
	void checkImplicitResourceDeclarations(ActorContainerClass context) {
		Set<String> seen = new HashSet<String>();
		staticResourceHelpers.getResourcesOfType(context, StaticResourceHelpers.TYPE_IMPLICIT).forEach(annotation -> {
			String res = roomHelpers.getAttribute(annotation, StaticResourceHelpers.TYPE_IMPLICIT);
			if (!res.matches(REGEX_VALID_RESOURCE_NAME)) {
				error("Implicit resource name contains invalid characters or starts with a digit", annotation,
						BasePackage.eINSTANCE.getAnnotation_Attributes());
			}
			if (seen.contains(res)) {
				warning("Implicit resource \"" + res + "\" is already declared in this actor", annotation,
						BasePackage.eINSTANCE.getAnnotation_Attributes());
			} else {
				seen.add(res);
			}
		});
	}

	@Check(CheckType.FAST)
	void checkUsesResourceReplicatedRefs(ActorContainerClass context) {
		for (ActorContainerRef ref : context.getActorRefs()) {
			ActorContainerClass refType = getActorContainerClass(ref.getStructureClass());
			Set<String> resourceNames = getAllResourceNames(refType);

			if (!resourceNames.isEmpty() && isReplicated(ref)) {
				error("ActorClass that uses an implicit static resource must have multiplicity of 1", ref,
						RoomPackage.eINSTANCE.getActorContainerRef_Name());
			}
		}
	}

	@Check(CheckType.NORMAL)
	void checkUsesImplicitResource(LogicalSystem sys) {
		// Collect resource usages in system
		HashMap<QualifiedName, Set<String>> usesResourcesByType = new HashMap<QualifiedName, Set<String>>();
		HashMap<String, ActorRefWalker.ActorRefNode> firstUsedMapping = new HashMap<String, ActorRefWalker.ActorRefNode>();

		for (ActorRefWalker.ActorRefNode node : new ActorRefWalker(sys, roomHelpers)) {
			ActorContainerClass refType = getActorContainerClass(node.getRef().getStructureClass());
			if (refType == null || node.getRef().eContainer() == null) {
				continue;
			}

			// Collect resources of current reference
			QualifiedName refTypeName = nameProvider.getFullyQualifiedName(refType);
			Set<String> resourceNames = usesResourcesByType.computeIfAbsent(refTypeName,
					name -> getAllResourceNames(refType));

			// Collect and check first used references
			for (String resourceName : resourceNames) {
				ActorRefWalker.ActorRefNode firstUsedNode = firstUsedMapping.putIfAbsent(resourceName, node);
				if (firstUsedNode != null) {
					String msg = getMessageAlreadyUsed(resourceName, node, firstUsedNode, sys);
					error(msg, sys, RoomPackage.eINSTANCE.getRoomClass_Name());
				}
			}
		}
	}

	private Set<String> getAllResourceNames(ActorContainerClass context) {
		return staticResourceHelpers.getTransitiveResourcesOfType(context, StaticResourceHelpers.TYPE_IMPLICIT)
				.map(annotation -> roomHelpers.getAttribute(annotation, StaticResourceHelpers.TYPE_IMPLICIT))
				.collect(Collectors.toSet());
	}

	private boolean isReplicated(ActorContainerRef ref) {
		return RoomPackage.eINSTANCE.getActorRef().isInstance(ref) && ((ActorRef) ref).getMultiplicity() != 1;
	}

	private String getMessageAlreadyUsed(String resource, ActorRefWalker.ActorRefNode target,
			ActorRefWalker.ActorRefNode usedByRef, LogicalSystem context) {
		assert usedByRef.getRef() != null;
		assert target.getRef() != null;
		String targetType = getTypeName(target.getRef().getStructureClass());
		String usedByType = getTypeName(usedByRef.getRef().getStructureClass());
		String msg = "Resource \"" + resource + "\" in ref " + target.getPath().toString("/") + ":" + targetType
				+ " is already used by another ref in system " + context.getName() + " (first encountered in "
				+ usedByRef.getPath().toString("/") + ":" + usedByType + ")";
		return msg;
	}

	private String getTypeName(StructureClass sc) {
		if (sc == null) {
			return "(unknown type)";
		}
		RoomModel model = EcoreUtil2.getContainerOfType(sc, RoomModel.class);
		String packagePrefix = (model != null && !model.eIsProxy()) ? model.getName() + "." : "(unknown package) ";
		String className = sc.getName() != null ? sc.getName() : "(unknown type)";
		return packagePrefix + className;
	}

	private ActorContainerClass getActorContainerClass(EObject object) {
		if (RoomPackage.eINSTANCE.getActorContainerClass().isInstance(object)) {
			return (ActorContainerClass) object;
		}
		return null;
	}

	@Override
	public void register(EValidatorRegistrar registrar) {
		// library validator is not registered for a specific language
	}

	/**
	 * Iterable that traverses the ActorContainerRef tree from a given root.
	 * Traversal includes ActorRefs from all inherited base ActorClasses. If an
	 * ActorRef is not valid then it will not be included.
	 */
	public static class ActorRefWalker implements Iterable<ActorRefWalker.ActorRefNode> {
		public static class ActorRefNode {
			private final QualifiedName path;
			private final ActorContainerRef ref;

			public ActorRefNode(QualifiedName path, ActorContainerRef ref) {
				this.path = path;
				this.ref = ref;
			}

			public QualifiedName getPath() {
				return this.path;
			}

			public ActorContainerRef getRef() {
				return this.ref;
			}
		}

		private StructureClass root;
		private RoomHelpers roomHelpers;

		public ActorRefWalker(StructureClass root, RoomHelpers roomHelpers) {
			this.root = root;
			this.roomHelpers = roomHelpers;
		}

		@Override
		public Iterator<ActorRefNode> iterator() {
			return new ActorRefWalkerIterator(root, roomHelpers);
		}

		/**
		 * Simple depth-first traversal iterator for ActorRef trees. Only follows
		 * "valid" refs, i.e. non-null non-proxy resolvable Refs that have resolvable
		 * StructureClass types. Short-circuits cyclic ActorRefs (StructureClass that
		 * appears more than once in containment hierarchy).
		 */
		public static class ActorRefWalkerIterator implements Iterator<ActorRefNode> {

			public static final int MAX_DEPTH = 1000; // hard-limit to mitigate infinite recursion

			RoomHelpers roomHelpers;

			// node traversal stack (LIFO for DFS traversal)
			Deque<ActorRefNode> stack = new LinkedList<ActorRefNode>();

			// tracks the classes of ActorRefs in the containment hierarchy to detect cycles
			Deque<StructureClass> containment = new LinkedList<StructureClass>();

			public ActorRefWalkerIterator(StructureClass root, RoomHelpers roomHelpers) {
				this.roomHelpers = roomHelpers;
				for (ActorContainerRef ref : roomHelpers.getAllActorContainerRefs(root)) {
					if (isRefValid(ref)) {
						stack.push(new ActorRefNode(QualifiedName.create(ref.getName()), ref));
					}
				}
			}

			@Override
			public boolean hasNext() {
				return !stack.isEmpty();
			}

			@Override
			public ActorRefNode next() {
				if (!hasNext()) {
					throw new NoSuchElementException();
				}
				ActorRefNode node = stack.pop();
				StructureClass nodeClass = node.getRef().getStructureClass();
				// pop entries in containment hierarchy until parent of current node
				// (assumes DFS traversal)
				while (containment.size() > node.getPath().getSegmentCount() - 1) {
					containment.pop();
				}
				// add current node to containment hierarchy and check depth
				containment.push(nodeClass);
				if (containment.size() > MAX_DEPTH) {
					throw new RuntimeException("Actor instance traversal exceeds maximum depth of " + MAX_DEPTH);
				}

				for (ActorContainerRef subRef : roomHelpers.getAllActorContainerRefs(nodeClass)) {
					// only push sub ActorRef onto the traversal stack if it is valid and its class is
					// not already in containment hierarchy
					if (isRefValid(subRef) && !containment.contains(subRef.getStructureClass())) {
						stack.push(new ActorRefNode(node.getPath().append(subRef.getName()), subRef));
					}
				}
				return node;
			}

			private boolean isRefValid(ActorContainerRef ref) {
				return (ref != null) && (!ref.eIsProxy()) && (ref.getName() != null)
						&& (ref.getStructureClass() != null) && (!ref.getStructureClass().eIsProxy());
			}
		}
	}

	/**
	 * Hook into global ResourceDescription strategy to determine when external
	 * ResourceDescriptions are affected by model changes where UsesResource
	 * annotations are present. Currently this is only relevant for logical system
	 * validation, so only candidates containing LogicalSystems are considered.
	 *
	 * NOTE: the isAffected criteria is very naive. There is e.g. currently no
	 * attempt to track if UsesResource annotations are changed from old to new
	 * within each delta - just the presence of any UsesResource annotation in the
	 * old or new state is enough.
	 */
	public static class StaticResourceValidatorIsAffected implements IsAffectedExtension {

		@Inject
		StaticResourceHelpers staticResourceHelper;

		@Override
		public boolean isAffected(Collection<Delta> deltas, IResourceDescription candidate,
				IResourceDescriptions context) {
			if (!candidate.getExportedObjectsByType(RoomPackage.eINSTANCE.getLogicalSystem()).iterator().hasNext()) {
				return false;
			}

			// check if delta has any new/existing container classes with UsesResources
			// annotations or any old ones that used to have UsesResource annotations
			for (Delta d : deltas) {
				if (d.haveEObjectDescriptionsChanged()) {
					Iterable<IEObjectDescription> newContainerClasses = d.getNew() == null ? Collections.emptyList()
							: d.getNew().getExportedObjectsByType(RoomPackage.eINSTANCE.getActorContainerClass());
					Iterable<IEObjectDescription> oldContainerClasses = d.getOld() == null ? Collections.emptyList()
							: d.getOld().getExportedObjectsByType(RoomPackage.eINSTANCE.getActorContainerClass());

					Set<IEObjectDescription> newNames = getMatchingDescriptions(newContainerClasses,
							staticResourceHelper::hasUsesResourceAnnotation);
					Set<IEObjectDescription> oldNames = getMatchingDescriptions(oldContainerClasses,
							staticResourceHelper::hasUsesResourceAnnotation);

					if ((oldNames.size() > 0) || (newNames.size() > 0)) {
						return true;
					}
				}
			}

			return false;
		}

		private Set<IEObjectDescription> getMatchingDescriptions(Iterable<IEObjectDescription> descriptions,
				Function<IEObjectDescription, Boolean> predicate) {
			Set<IEObjectDescription> result = new HashSet<IEObjectDescription>();
			for (IEObjectDescription d : descriptions) {
				if (predicate.apply(d)) {
					result.add(d);
				}
			}
			return result;
		}
	}
}

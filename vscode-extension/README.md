# eTrice VSCode Extension

🚧 This extension is a preview.

## Installation
 * Prerequisite: Java 11.
 * Build the extension packaged as vsix file with `../gradlew vscePackage`.
 * Use the command `Install from VSIX` in Visual Studio Code to install the extension.

## Usage
Add every project as workspace folder.
Make sure that there is a modelpath description file in the top level directory of each project.

## Features
 * Basic syntax highlighting
 * eTrice language server

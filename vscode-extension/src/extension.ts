'use strict';

import * as path from 'path';
import * as os from 'os';
import * as net from 'net';

import { Trace } from 'vscode-jsonrpc';
import { workspace, ExtensionContext } from 'vscode';
import { LanguageClient, LanguageClientOptions, ServerOptions, StreamInfo } from 'vscode-languageclient';

export function activate(context: ExtensionContext) {
    let serverOptions = localServerOptions(context)
    
    let clientOptions: LanguageClientOptions = {
        documentSelector: ['room', 'etphys', 'etmap', 'cage'],
        synchronize: {
            fileEvents: workspace.createFileSystemWatcher('**/*')
        }
    };

    // Create the language client and start the client.
    let lc = new LanguageClient('etrice', 'eTrice Language Server', serverOptions, clientOptions);
    let disposable = lc.start();

    // Push the disposable to the context's subscriptions so that the 
    // client can be deactivated on extension deactivation
    context.subscriptions.push(disposable);
}

// The server is a locally installed in the directory 'etrice'
function localServerOptions(context: ExtensionContext): ServerOptions {
    let launcher = os.platform() === 'win32' ? 'etrice.bat' : 'etrice';
    let script = context.asAbsolutePath(path.join('etrice', 'bin', launcher));
    return {
        run: { command: script },
        debug: { command: script, args: [], options: { env: createDebugEnv() } }
    };
}

function createDebugEnv() {
    return Object.assign({
        JAVA_OPTS: "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,quiet=y,address=5005"
    }, process.env);
}

// The server is a started as a separate app and listens on port 5008
function socketServerOptions(): ServerOptions {
    let connectionInfo = {
        port: 5008
    };
    return () => {
        // Connect to language server via socket
        let socket = net.connect(connectionInfo);
        let result: StreamInfo = {
            writer: socket,
            reader: socket
        };
        return Promise.resolve(result);
    };
}